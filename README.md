# Take Home

- Forge Rock Scatterplot

Additional notes:

1. Original plan was to build a scatterplot with d3 in a more naive way and then reformat it, but ran out of time
   - A much better approach would be to take the current scatterplot and break it into react components. Likewise the rendering would be done by React instead of D3, but use D3 for scale generation and data munging
2. In the Interview with Sudhakar, we discussed breakpoints, flexbox & grid, so I used those here for the layout. I added some media queries for certain elements and added color gradients to show boundaries.
3. There are still some visual issues with the chart, including rough scaling for chart axis size, so that at some screen sizes the drawn circles would fall outside of the bounds.
4. There are also some issues with this approach and layering it in a use effect, including the need for a setTimeout which was unfortunate and also some interesting issues with typescript.
