import { Link } from "react-router-dom";

const Header = () => {
  return (
    <header className="h-40 flex items-center bg-gradient-to-r from-green-400 to-yellow-500">
      <Link to="/" className="p-5 ml-5 bg-purple-500 rounded">
        Dashboard
      </Link>
    </header>
  );
};

export default Header;
