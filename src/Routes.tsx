import { Switch, Route } from "react-router-dom";

import Dashboard from "./Dashboard";

const Routes = () => {
  return (
    <Switch>
      <Route path="/">
        <Dashboard />
      </Route>
    </Switch>
  );
};

export default Routes;
