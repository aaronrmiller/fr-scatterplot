const Footer = () => {
  return (
    <footer className="h-40 flex justify-center items-center bg-gradient-to-r from-indigo-400 to-pink-600">
      Footer
    </footer>
  );
};

export default Footer;
