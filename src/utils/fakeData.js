const { build, fake, sequence, oneOf } = require("@jackfranklin/test-data-bot");

export const userBuilder = build({
  fields: {
    id: sequence(),
    firstName: fake((f) => f.name.firstName()),
    lastName: fake((f) => f.name.lastName()),
    age: fake((f) => f.random.number({ min: 1, max: 75 })),
    weight: fake((f) => f.random.number({ min: 120, max: 300 })),
    height: fake((f) => f.random.number({ min: 150, max: 200 })),
    streetAdd: fake((f) => f.address.streetAddress()),
    zipCode: fake((f) => f.address.zipCode()),
    state: fake((f) => f.address.state()),
    sex: oneOf("male", "female"),
    city: fake((f) => f.address.city()),
    country: fake((f) => f.address.country()),
  },
});

export const fakeUserData = Array.from({ length: 20 }, (_, i) => userBuilder());

export const generateFakeUserData = (numberOfUsers) =>
  Array.from({ length: numberOfUsers }, (_, i) => userBuilder());
