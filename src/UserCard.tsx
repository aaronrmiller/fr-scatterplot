import { UserData } from "./Dashboard";

interface UserCardProps {
  userData: UserData;
  setUserData: React.Dispatch<React.SetStateAction<UserData | null>>;
}
// const UserCard = ({ userData }: UserCard) => (
const UserCard = ({
  userData: { firstName, lastName, sex, streetAdd, city, country, state },
  setUserData,
}: UserCardProps) => (
  <div className="border-2 border-yellow-600 rounded h-full flex flex-col justify-between items-center">
    <table>
      <thead>
        <tr className="text-left">
          <th>User Information:</th>
        </tr>
        <tr className="text-left">
          <th className="">First Name</th>
          <th>Last Name</th>
          <th>Sex</th>
        </tr>
      </thead>
      <tbody>
        <tr className="text-left">
          <td>{firstName}</td>
          <td>{lastName}</td>
          <td>{sex}</td>
        </tr>
      </tbody>
      <thead>
        <tr className="text-left">
          <th>User Location:</th>
        </tr>
        <tr className="text-left">
          <th>StreetAdd</th>
          <th>City</th>
          <th>Country</th>
          <th>State</th>
        </tr>
      </thead>
      <tbody>
        <tr className="text-left">
          <td>{streetAdd}</td>
          <td>{city}</td>
          <td>{country}</td>
          <td>{state}</td>
        </tr>
      </tbody>
    </table>
    <button
      className="bg-red-500 w-20 h-20 rounded"
      onClick={() => setUserData(null)}
    >
      Remove Card
    </button>
  </div>
);
export default UserCard;
