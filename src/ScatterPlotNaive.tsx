import { useEffect, useRef } from "react";
import { UserData } from "./Dashboard";
import {
  select,
  extent,
  scaleLinear,
  axisBottom,
  axisLeft,
  BaseType,
} from "d3";

interface ScatterPlotNaiveProps {
  usersData: UserData[];
  width: number;
  height: number;
  setUserData: React.Dispatch<React.SetStateAction<UserData | null>>;
}
interface ScatterPlotDimensions {
  width: number;
  height: number;
  margin: {
    top: number;
    right: number;
    bottom: number;
    left: number;
  };
  boundedWidth?: number;
  boundedHeight?: number;
}

const ScatterPlotNaive = ({
  usersData,
  width,
  height,
  setUserData,
}: ScatterPlotNaiveProps) => {
  const ref = useRef<SVGSVGElement | null>(null);
  const sortedUsersData = usersData.sort((userA, userB) => {
    if (userA.age === userB.age) return 0;
    if (userA.age > userB.age) return -1;
    return 1;
  });
  useEffect(() => {
    const xAccessor = (d: UserData): number => d.height;
    const yAccessor = (d: UserData): number => d.weight;

    const dimensions: ScatterPlotDimensions = {
      width,
      height,
      margin: {
        top: 10,
        right: 20,
        bottom: 50,
        left: 50,
      },
    };
    dimensions.boundedWidth =
      dimensions.width - dimensions.margin.left - dimensions.margin.right;
    dimensions.boundedHeight =
      dimensions.height - dimensions.margin.top - dimensions.margin.bottom;
    const minMaxX = extent(usersData, xAccessor) as [number, number];
    const minMaxXAdj = [minMaxX[0] - 10, minMaxX[1] + 10];
    const xScale = scaleLinear()
      .domain(minMaxXAdj)
      .range([0, dimensions.boundedWidth])
      .nice();

    const minMaxY = extent(sortedUsersData, yAccessor) as [number, number];
    const minMaxYAdj = [minMaxY[0] - 30, minMaxY[1] + 30];
    const yScale = scaleLinear()
      .domain(minMaxYAdj)
      .range([dimensions.boundedHeight, 0])
      .nice();

    const svg = select(ref.current)
      .attr("width", dimensions.width)
      .attr("height", dimensions.height);

    svg.selectAll("*").remove();
    const bounds = svg
      .append("g")
      .attr("clip-path", "url(#bounds-clip-path)")
      .style(
        "transform",
        `translate(${dimensions.margin.left}px, ${dimensions.margin.top}px)`
      );

    const gDots = bounds
      .selectAll("g")
      .data(sortedUsersData)
      .enter()
      .append("g");

    // eslint-disable-next-line
    const circles = gDots
      .append("circle")
      .attr("cx", (d) => xScale(xAccessor(d)))
      .attr("cy", (d) => yScale(yAccessor(d)))
      .attr("r", (d) => d.age)
      .attr("fill", "transparent")
      .attr("stroke", "black")
      .attr("fill", (d) => {
        return d.age < 25 ? "#EAA771" : d.age < 50 ? "#DF7C2A" : "#7C4213";
      });

    const infoWindow = select("#infoWindow");
    const onMouseEnter = (event: MouseEvent, d: UserData) => {
      console.log({ event, d });
      console.log(event.currentTarget);
      console.log(event.target);
      //   TODO: Determine how to type this
      // This appears to be the correct way to use this per Bostock: https://github.com/d3/d3-selection/issues/263#issuecomment-699585344
      const hoveredEle = select(event.currentTarget as BaseType);
      console.log(hoveredEle);
      const x = xScale(xAccessor(d));
      const y = yScale(yAccessor(d));
      infoWindow.attr(
        "class",
        "absolute flex flex-col items-center justify-center h-32 w-32 rounded bg-red-500"
      );
      infoWindow.append("p").html(`Info Window`);
      const list = infoWindow.append("ul");
      list.append("li").html(`Height: ${d.height}`);
      list.append("li").html(`Weight: ${d.weight}`);
      list.append("li").html(`Age: ${d.age}`);

      const infoWindowXTranslation = d.height > 180 ? -50 : 50;
      const infoWindowYTranslation = d.weight > 270 ? 50 : -100;

      infoWindow.style(
        "transform",
        `translate(calc(${infoWindowXTranslation}% + ${x}px), calc(${infoWindowYTranslation}% + ${y}px))`
      );
      console.log({ infoWindow, hoveredEle });
    };
    const onMouseLeave = (event: MouseEvent, d: UserData) => {
      infoWindow.selectAll("*").remove();
      infoWindow.attr("class", "hidden");
    };

    // TODO: Determine appropriate type, compiler was being finicky about this one
    // const onClick = (event: any, d: unknown) => {
    const onClick = (event: any, d: UserData) => {
      // TODO: Determine why tooltip hangs
      // Really unfortunate to use this, but didn't have time to debug further
      setTimeout(() => {
        infoWindow.attr("class", "hidden opacity-0");
        infoWindow.selectAll("*").remove();
        return;
      }, 0);
      setUserData(d as UserData);
    };

    gDots
      .on("mouseover", onMouseEnter)
      .on("mouseout", onMouseLeave)
      .on("click", onClick);

    // eslint-disable-next-line
    const label = gDots
      .append("text")
      // TODO: Needs better scaling, likely util func
      .attr("x", (d) => xScale(xAccessor(d)) + d.age)
      .attr("y", (d) => yScale(yAccessor(d)) - d.age)
      .attr("fill", "black")
      .attr("stroke", "black")
      .attr("class", "text-xl bg-red-500")
      .text((d) => d.firstName);

    const xAxisGenerator = axisBottom(xScale);
    const xAxis = bounds
      .append("g")
      .call(xAxisGenerator)
      .style("transform", `translateY(${dimensions.boundedHeight}px)`);

    // eslint-disable-next-line
    const xAxisLabel = xAxis
      .append("text")
      .attr("x", dimensions.boundedWidth / 2)
      .attr("y", dimensions.margin.bottom - 10)
      .attr("fill", "black")
      .style("font-size", "1rem")
      .text("Height");

    const yAxisGenerator = axisLeft(yScale);

    const yAxis = bounds.append("g").call(yAxisGenerator);
    // eslint-disable-next-line
    const yAxisLabel = yAxis
      .append("text")
      .attr("x", -dimensions.boundedHeight / 2)
      .attr("y", -dimensions.margin.left + 20)
      .attr("fill", "black")
      .style("font-size", "1rem")
      .text("Weight")
      .style("transform", "rotate(-90deg)")
      .style("text-anchor", "middle");
  }, [width, height, sortedUsersData, usersData, setUserData]);
  return (
    <>
      <svg ref={ref} />
    </>
  );
};

export default ScatterPlotNaive;
