import "./App.css";
import Routes from "./Routes";
import Header from "./Header";
import Footer from "./Footer";

function App() {
  return (
    <div className="h-screen grid grid-rows-pageLayout">
      <Header />
      <div className="h-full grid grid-flow-col grid-cols-6">
        <aside className="h-full col-span-2 hidden md:flex justify-center items-center bg-gradient-to-r from-gray-700 to-blue-600">
          Navigation Menu Left
        </aside>
        <div className="h-full col-span-6 md:col-span-4">
          <Routes />
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default App;
