import { useState } from "react";
import useElementSize from "@charlietango/use-element-size";

import ScatterPlotNaive from "./ScatterPlotNaive";
import { generateFakeUserData } from "./utils/fakeData.js";
import UserCard from "./UserCard";
export interface UserData {
  id: string;
  firstName: string;
  lastName: string;
  age: number;
  weight: number;
  height: number;
  streetAdd: string;
  zipCode: number;
  state: string;
  sex: string;
  city: string;
  country: string;
}
const Dashboard = () => {
  const [usersData, setUsersData] = useState<UserData[]>(
    (): UserData[] => generateFakeUserData(10) as UserData[]
  );
  const [userData, setUserData] = useState<UserData | null>(usersData[0]);
  const [sectionContainerRef, { width, height }] = useElementSize();
  const [
    chartContainerRef,
    { width: chartContainerRefWidth, height: chartContainerRefHeight },
  ] = useElementSize();
  console.log({ width, height });
  return (
    <div className="h-full flex flex-col justify-center">
      <div
        ref={sectionContainerRef}
        className="flex-grow grid grid-flow-col grid-cols-6"
      >
        <div
          className={`relative ${
            userData ? "col-start-1 col-end-4" : "col-start-1 col-end-7"
          }`}
          ref={chartContainerRef}
        >
          <div id="infoWindow" className="hidden" />
          <ScatterPlotNaive
            width={userData ? chartContainerRefWidth : width}
            height={chartContainerRefHeight}
            usersData={usersData}
            setUserData={setUserData}
          />
        </div>
        <div className={`${userData ? "col-start-4 col-end-7" : ""}`}>
          {userData && (
            <UserCard userData={userData} setUserData={setUserData} />
          )}
        </div>
      </div>
      <button
        className="bg-green-500 rounded w-20 h-20 m-auto"
        onClick={() => setUsersData(generateFakeUserData(10) as UserData[])}
      >
        Generate Data
      </button>
    </div>
  );
};

export default Dashboard;
